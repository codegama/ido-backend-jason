<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRewardAmountValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('staked_amount')->after('password')->default(0);
        });

        DB::statement('ALTER TABLE projects MODIFY COLUMN `exchange_rate` DOUBLE(20,18) DEFAULT  0.000000000000000000');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('staked_amount');
        });

        DB::statement('ALTER TABLE projects MODIFY COLUMN `exchange_rate` DOUBLE(16,4) DEFAULT 0.00');

    }
}
