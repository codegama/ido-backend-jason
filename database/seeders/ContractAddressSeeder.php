<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContractAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->insert([
            [
                'key' => 'lp_contract_address',
                'value' => '0xe9e7cea3dedca5984780bafc599bd69add087d56'
            ],
            [
                'key' => 'project_contract_address',
                'value' => '0xf729f4d13a9c9556875d20bacf9ebd0bf891464c'
            ],
        ]);
    }
}
