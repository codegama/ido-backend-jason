@extends('layouts.admin')

@section('title', tr('users'))

@section('content-header', tr('users'))

@section('breadcrumb')

    <li class="breadcrumb-item active">
        <a href="{{route('admin.users.index')}}">{{ tr('users') }}</a>
    </li>

    <li class="breadcrumb-item">{{$title ?? tr('view_users')}}</li>

@endsection

@section('content')

<div class="row">   

    <div class="col-md-12">
         
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">{{$title ?? tr('view_users')}}</h3>

            </div>

            <!-- /.box-header -->
            <div class="box-body">

                <div class="callout bg-pale-secondary">
                    <h4>Notes:</h4>
                    <p>
                        <ul>
                            <li>User can be both Project owner(The person who add the token for presale or IDO) and Investor(The person who invest the project token or The tokens added by the Project owner)</li>
                            <li>User A can be a project owner as well as investor. </li>
                            <li>User A invest in any of the project token - Then User A will be investor. </li>
                            <li>User A add any project into launch page - Then user A will be Project owner. </li>
                        </ul>
                    </p>
                </div>

                <form method="GET" action="{{route('admin.users.staked_amount')}}">

                    <div class="row">


                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 resp-mrg-btm-md">
                            @if(Request::has('search_key'))
                            <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
                            @endif
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-3 col-md-3 offset-lg-3 offset-md-3 md-full-width resp-mrg-btm-md">

                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">

                            <div class="input-group">

                                <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key')??''}}" placeholder="{{tr('users_search_placeholder')}}"> 

                                <span class="input-group-btn">
                                    &nbsp

                                    <button type="submit" class="btn btn-default reset-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>

                                    <a href="{{route('admin.users.staked_amount')}}" class="btn btn-default reset-btn">
                                        <span> <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </span>
                                    </a>

                                </span>

                            </div>

                        </div>

                    </div>

                </form>
                <br>

                <div class="table-responsive">
                    
                    <table id="example2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                        <thead>
                            <tr>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('name')}}</th>
                                <th>{{tr('wallet_address')}}</th>
                                <th>{{tr('staked_amount')}}</th>
                                <th>{{tr('tier')}}</th>
                                <th>{{tr('action')}}</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($users as $i => $user)

                                <tr>

                                    <td>{{ $i+$users->firstItem() }}</td>

                                    <td class="white-space-nowrap">
                                        <a href="{{route('admin.users.view' , ['user_id' => $user->id])}}" class="custom-a">
                                            {{$user->name ?: tr('not_available')}}
                                        </a>
                                        
                                    </td>

                                    <td>
                                        {{ $user->wallet_address ?: tr('not_available') }}
                                    </td>

                                    <td>
                                        {{ $user->staked_amount ?? tr('not_available') }} BUSDX
                                    </td>

                                    <td>
                                        <a href="{{ (subscription_tire($user->staked_amount ?? 0)['subscription_link']) ?? tr('n_a') }}" class="custom-a">
                                            {{ (subscription_tire($user->staked_amount ?? 0)['tire']) ?? tr('n_a') }}
                                        </a>
                                    </td>
                                   
                                    <td>

                                        <div class="btn-group" role="group">

                                            <a href="{{route('admin.users.view' , ['user_id' => $user->id])}}" class="btn btn-primary"><i class="ft-settings icon-left"></i> {{ tr('view') }}</a>

                                        </div>

                                    </td>

                                </tr>
                                
                            @endforeach                         
                        </tbody>                       
                    </table>
                
                </div>

            </div>

            <div class="box-footer clearfix">
                    
                <div class="pull-right rd-flex">
                    {{$users->appends(request()->input())->links('pagination::bootstrap-4')}}
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
